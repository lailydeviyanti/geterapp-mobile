import IconDampak from './Dampak.svg';
import IconKedalaman from './Kedalaman.svg';
import IconKoordinat from './Koordinat.svg';
import IconLokasi from './Lokasi.svg';
import IconRecent from './Recent.svg';
import IconSkala from './Skala.svg';

export {
    IconDampak, IconKedalaman, IconKoordinat, IconLokasi, IconRecent, IconSkala
}