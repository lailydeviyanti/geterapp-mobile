import { StyleSheet, Text, View } from 'react-native'
import React, { useEffect } from 'react'

const Splash = ({navigation}) => {
    useEffect(() => {
      setTimeout( () => {
        navigation.replace('Home');
      }, 2000);
  }, [navigation]);

  return (
    <View style={styles.body}>
      <Text style={{fontSize: 50, color: 'white'}}>GeterApp</Text>
      <Text style={{color: 'white', letterSpacing: 5, marginTop: 5,}}>By Laily Devi Yanti</Text>
    </View>
  )
}

export default Splash

const styles = StyleSheet.create({
    body: {
        backgroundColor: '#4a8cfa',
        width: '100%',
        height: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})