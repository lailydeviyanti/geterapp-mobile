import { Image, StyleSheet, Text, View } from 'react-native'
import React, { useEffect, useState } from 'react'
import axios from 'axios';
import { BASE_URL } from '../../utils/api';
import { IconDampak, IconKedalaman, IconKoordinat, IconLokasi, IconRecent, IconSkala } from '../../resource';

const Home = () => {
    const [data, setData] = useState([]);
    const [tanggal, setTanggal] = useState("");
    const [Jam, setJam] = useState("");
    const [dampak, setDampak] = useState("");
    const [pusat, setPusat] = useState("");
    const [Kedalaman, setKedalaman] = useState("");
    const [Skala, setSkala] = useState("");
    const [smap, setSmap] = useState("");
    const [Koordinat, setKoordinat] = useState("");
    const [bujur, setbujur] = useState("");
    const [lintang, setlintang] = useState("");

    useEffect(() => {
        GetData();
    }, [])

    const GetData = async () => {
        let res = await axios.get(`https://cuaca-gempa-rest-api.vercel.app/quake`);
        if (res) {
            console.log(res.data.data);
            setTanggal(res.data.data.tanggal);
            setJam(res.data.data.jam);
            setDampak(res.data.data.dirasakan);
            setPusat(res.data.data.wilayah);
            setKedalaman(res.data.data.kedalaman);
            setSkala(res.data.data.magnitude);
            setSmap(res.data.data.shakemap);
            setKoordinat(res.data.data.coordinates);
            setbujur(res.data.data.bujur);
            setlintang(res.data.data.lintang);
        }
    }
  return (
    <View style={styles.body}>
        <View style={styles.header}>
            <Text style={{fontSize: 20, fontWeight: 'bold', color: '#4a8cfa', letterSpacing: 1,}}>GeterApp</Text>
        </View>
        <View style={styles.map}>
            <Image source={{ uri: smap}} style={{width: 400, height: 500}} />
        </View>
        <View style={styles.baris}>
            <View style={{alignItems: 'center',}}>
                <IconRecent />
                <Text style={styles.txt}>{tanggal}</Text>
                <Text style={styles.txt}>{Jam}</Text>
            </View>
            <View style={{alignItems: 'center',}}>
                <IconSkala />
                <Text style={styles.txt}>
                    {Skala} SR
                </Text>
                <Text style={styles.txt}>Magnitude</Text>
            </View>
            <View style={{alignItems: 'center',}}>
                <IconKedalaman />
                <Text style={styles.txt}>{Kedalaman}</Text>
                <Text style={styles.txt}>Kedalaman</Text>
            </View>
        </View>
        <View style={{padding: 30, backgroundColor: 'white',}}>
            <View style={{marginBottom: 5, display: 'flex', flexDirection: 'row'}}>
                <View style={{justifyContent: 'center', marginRight: 10,}}>
                    <IconKoordinat />
                </View>
                <View >
                    <Text>Koordinat</Text>
                    <Text style={styles.txt2}>{Koordinat} | {bujur} , {lintang}</Text>
                </View>
            </View>
            <View style={{marginBottom: 5, display: 'flex', flexDirection: 'row',}}>
                <View style={{justifyContent: 'center', marginRight: 10,}}>
                    <IconLokasi />
                </View>
                <View>
                    <Text>Lokasi</Text>
                    <Text style={styles.txt2}>{pusat}</Text>
                </View>
            </View>
            <View style={{marginBottom: 5, display: 'flex', flexDirection: 'row',}}>
                <View style={{justifyContent: 'center', marginRight: 10,}}>
                    <IconDampak />
                </View>
                <View>
                    <Text>Daerah Yang Terdampak</Text>
                    <Text style={styles.txt2}>{dampak}</Text>
                </View>
            </View>
            
        </View>
    </View>
  )
}

export default Home

const styles = StyleSheet.create({
    body: {
        backgroundColor: 'white',
        width: '100%',
        height: '100%',
    },
    header: {
        borderBottomColor: 'grey',
        borderBottomWidth: 1,
        padding: 15,
        alignItems: 'center',
        width: '100%',
        // height: 50,
    },
    map: {
        width: '100%',
        height: 392,
        backgroundColor: 'red',
    },
    baris: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: 'white',
    },
    txt: {
        fontWeight: '400',
        color: 'black',
    },
    txt2: {
        fontWeight: '400',
        color: 'black',
        width: 300,
    }
})